include (CheckCCompilerFlag)
include (CheckCXXCompilerFlag)

option (USE_THREAD_SANITIZER "Use ThreadSanitizer" OFF)

if (USE_THREAD_SANITIZER)
  set (THREAD_SANITIZER_FLAGS "-fsanitize=thread")
  list (APPEND CMAKE_REQUIRED_LIBRARIES tsan)

  check_c_compiler_flag   (${THREAD_SANITIZER_FLAGS} C_HAS_THREAD_SANITIZER)
  if (NOT C_HAS_THREAD_SANITIZER)
    message (FATAL_ERROR "Your C compiler does not support ThreadSanitizer")
  endif ()

  check_cxx_compiler_flag (${THREAD_SANITIZER_FLAGS} CXX_HAS_THREAD_SANITIZER)
  if (NOT CXX_HAS_THREAD_SANITIZER)
    message (FATAL_ERROR "Your C++ compiler does not support ThreadSanitizer")
  endif ()

  set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${THREAD_SANITIZER_FLAGS}")
  set (CMAKE_C_FLAGS   "${CMAKE_C_FLAGS} ${THREAD_SANITIZER_FLAGS}")
endif ()
