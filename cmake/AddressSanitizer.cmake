include (CheckCCompilerFlag)
include (CheckCXXCompilerFlag)

option (USE_ADDRESS_SANITIZER "Use AddressSanitizer" OFF)

if (USE_ADDRESS_SANITIZER)
  set (ADDRESS_SANITIZER_FLAGS "-fsanitize=address")
  list (APPEND CMAKE_REQUIRED_LIBRARIES asan)

  check_c_compiler_flag   (${ADDRESS_SANITIZER_FLAGS} C_HAS_ADDRESS_SANITIZER)
  if (NOT C_HAS_ADDRESS_SANITIZER)
    message (FATAL_ERROR "Your C compiler does not support AddressSanitizer")
  endif ()

  check_cxx_compiler_flag (${ADDRESS_SANITIZER_FLAGS} CXX_HAS_ADDRESS_SANITIZER)
  if (NOT CXX_HAS_ADDRESS_SANITIZER)
    message (FATAL_ERROR "Your C++ compiler does not support AddressSanitizer")
  endif ()

  set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${ADDRESS_SANITIZER_FLAGS}")
  set (CMAKE_C_FLAGS   "${CMAKE_C_FLAGS} ${ADDRESS_SANITIZER_FLAGS}")
endif ()
