################################################################################
#
# CMake script for finding Log4cxx.
# The default CMake search process is used to locate files.
#
# This script creates the following variables:
#  JEMALLOC_FOUND: Boolean that indicates if the package was found
#  JEMALLOC_INCLUDE_DIRS: Paths to the necessary header files
#  JEMALLOC_LIBRARIES: Package libraries
#  JEMALLOC_LIBRARY_DIRS: Path to package libraries
#
################################################################################

include (FindPackageHandleStandardArgs)

# See if JEMALLOC_ROOT is not already set in CMake
if (NOT JEMALLOC_ROOT)
  # See if JEMALLOC_ROOT is set in process environment
  if (NOT $ENV{JEMALLOC_ROOT} STREQUAL "")
    set (JEMALLOC_ROOT "$ENV{JEMALLOC_ROOT}")
  message (STATUS "Detected JEMALLOC_ROOT set to '${JEMALLOC_ROOT}'")
    endif ()
endif ()

# If JEMALLOC_ROOT is available, set up our hints
if (JEMALLOC_ROOT)
    set (JEMALLOC_INCLUDE_HINTS HINTS "${JEMALLOC_ROOT}/include" "${JEMALLOC_ROOT}")
    set (JEMALLOC_LIBRARY_HINTS HINTS "${JEMALLOC_ROOT}/lib")
endif ()

# Find headers and libraries
find_path (JEMALLOC_INCLUDE_DIR NAMES jemalloc/jemalloc.h ${JEMALLOC_INCLUDE_HINTS})
find_library (JEMALLOC_LIBRARY NAMES jemalloc ${JEMALLOC_LIBRARY_HINTS})
find_library (JEMALLOCD_LIBRARY NAMES jemalloc${CMAKE_DEBUG_POSTFIX} ${JEMALLOC_LIBRARY_HINTS})

# Set JEMALLOC_FOUND honoring the QUIET and REQUIRED arguments
find_package_handle_standard_args (JEMALLOC DEFAULT_MSG JEMALLOC_LIBRARY JEMALLOC_INCLUDE_DIR)

# Output variables
if (JEMALLOC_FOUND)
  # Include dirs
  set (JEMALLOC_INCLUDE_DIRS ${JEMALLOC_INCLUDE_DIR})

  # Libraries
  if (JEMALLOC_LIBRARY)
    set (JEMALLOC_LIBRARIES optimized ${JEMALLOC_LIBRARY})
  else (JEMALLOC_LIBRARY)
    set (JEMALLOC_LIBRARIES "")
  endif (JEMALLOC_LIBRARY)
  if (JEMALLOCD_LIBRARY)
    set (JEMALLOC_LIBRARIES debug ${JEMALLOCD_LIBRARY} ${JEMALLOC_LIBRARIES})
  endif (JEMALLOCD_LIBRARY)

  # Link dirs
  get_filename_component (JEMALLOC_LIBRARY_DIRS ${JEMALLOC_LIBRARY} PATH)
endif ()

# Advanced options for not cluttering the cmake UIs
mark_as_advanced (JEMALLOC_INCLUDE_DIR JEMALLOC_LIBRARY)
