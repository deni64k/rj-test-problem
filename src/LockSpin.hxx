#ifndef PROBLEM_SRC_LOCK_SPIN_H_
#define PROBLEM_SRC_LOCK_SPIN_H_

#include <atomic>
#include <cstdint>

#include "LockStats.hxx"

class LockSpin {
public:
  constexpr static std::size_t PAUSE_COUNT    = 16;
  constexpr static std::size_t SPINNING_LIMIT = PAUSE_COUNT * 128;

  LockSpin()
  : is_locked_(ATOMIC_FLAG_INIT)
  {}
  LockSpin(LockSpin const &) = delete;

  bool try_lock() {
    return !is_locked_.test_and_set();
  }

  void lock() {
    lock_wait();
  }

  bool lock_wait() {
    std::uint_fast32_t i = 0;
    std::uint_fast8_t  j = 0;
    while (is_locked_.test_and_set()) {
      ++i;
      if (++j >= PAUSE_COUNT) {
        std::this_thread::yield();
        j = 0;
      }
    }
    if (i > 0)
      LockStats::spinning_count += i;
    ++LockStats::spinlocks_aquired;
    if (++i >= SPINNING_LIMIT) {
      std::cerr << "LockSpin: " << "Too much spinning in LockSpin: " << i << " times." << std::endl;
    }

    return true;
  }

  void unlock() {
    return is_locked_.clear();
  }

private:
  std::atomic_flag is_locked_;
};

#endif
