#ifndef PROBLEM_SRC_RECORD_H_
#define PROBLEM_SRC_RECORD_H_

#include <string>

#include "Database.hxx"

// template <typename Key = std::string, typename Data = std::string>
class Record {
public:
  typedef std::string Key;
  typedef std::string Data;

  Record(Key const &key, Data const &data)
  : key_(key)
  , data_(data)
  {}
  Record(Key &&key, Data &&data)
  : key_(key)
  , data_(data)
  {}
  Record(Record &&other)
  : key_(std::move(other.key_))
  , data_(std::move(other.data_))
  {}

  Key  const & key()  const { return key_; }
  Data const & data() const { return data_; }
  bool is_dirty() const { return is_dirty_; }

  void set_data(Data const &data) { is_dirty_ = data_ != data; data_ = data; }
  void set_data(Data &&data)      { is_dirty_ = data_ != data; data_ = std::move(data); }
  void set_dirty(bool const is_dirty) { is_dirty_ = is_dirty; }

private:
  Key  key_;
  Data data_;
  bool is_dirty_;
};

#endif
