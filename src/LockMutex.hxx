#ifndef PROBLEM_SRC_LOCK_MUTEX_H_
#define PROBLEM_SRC_LOCK_MUTEX_H_

#include <boost/thread/mutex.hpp>
#include <boost/chrono/duration.hpp>

class LockMutex {
public:
  LockMutex()
  : mutex_()
  {}
  LockMutex(LockMutex const &) = delete;

  bool try_lock() {
    return mutex_.try_lock();
  }

  void lock() {
    lock_wait();
  }

  bool lock_wait() {
    if (!mutex_.try_lock_for(boost::chrono::seconds(10)))
      throw std::runtime_error("Too long awaiting a lock in LockMutex");
    return true;
  }

  void unlock() {
    return mutex_.unlock();
  }

private:
  boost::timed_mutex mutex_;
};

#endif
