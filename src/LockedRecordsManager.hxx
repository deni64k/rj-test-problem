#ifndef PROBLEM_SRC_RECORD_MANAGER_H_
#define PROBLEM_SRC_RECORD_MANAGER_H_

#include <map>
#include <functional>
#include <forward_list>
#include <mutex>
#include <memory>
#include <thread>

#include <tbb/concurrent_hash_map.h>
#include <tbb/spin_mutex.h>

#include "Record.hxx"
#include "Lock.hxx"
#include "AutoLock.hxx"

template <typename Key>
class LockedRecordsManager {
public:
  typedef std::tuple<Record::Data, bool> RecordLoaderResult;

  typedef typename tbb::concurrent_hash_map<Key, Record *>                 Records;
  typedef typename tbb::concurrent_hash_map<Key, Record *>::accessor       RecordAccessor;
  typedef typename tbb::concurrent_hash_map<Key, Record *>::const_accessor RecordConstAccessor;

  LockedRecordsManager(soci::session *sql)
  : sql_(sql)
  {
    // sql_->set_log_stream(&std::cerr);
  }

  ~LockedRecordsManager() {
    for (auto &pair : records_) {
      delete pair.second;
    }
  }

  void preload(std::size_t size) {
    soci::rowset<soci::row> rs = ((*sql_).prepare << "SELECT key, data FROM records LIMIT " << size);

    for (soci::rowset<soci::row>::const_iterator it = rs.begin(); it != rs.end(); ++it)
    {
      soci::row const &row = *it;
      Record         *record = new Record(row.get<std::string>(0), row.get<std::string>(1));
      RecordAccessor  accessor;
      if (!records_.insert(accessor, record->key()))
        delete accessor->second;
      accessor->second = record;
    }
  }

  template <typename Loader>
  bool load_record(Key const &key, RecordAccessor &accessor, Loader const &loader) {
    if (!records_.insert(accessor, key))
      return true;

    RecordLoaderResult loader_result(std::move(loader(key)));
    Record *record = new Record(key, std::move(std::get<0>(loader_result)));

    accessor->second = record;

    if (std::get<1>(loader_result))
      enqueue_dirty_record_(*record);

    return false;
  }

  bool load_record(Key const &key, RecordAccessor &accessor) {
    if (!records_.find(accessor, key))
      return true;

    return false;
  }

  bool load_record(Key const &key, RecordConstAccessor &accessor) {
    if (!records_.find(accessor, key))
      return true;

    return false;
  }

  // template <typename Loader>
  // bool load_record(Key const &key, RecordConstAccessor &caccessor, Loader const &loader) {
  //   RecordAccessor &accessor;
  //   if (!records_.insert(accessor, key))
  //     return true;

  //   RecordLoaderResult loader_result(std::move(loader(key)));
  //   Record *record = new Record(key, std::move(std::get<0>(loader_result)));

  //   accessor->second = record;

  //   if (std::get<1>(loader_result))
  //     mark_dirty_(record);

  //   return false;
  // }

  template <typename Storer>
  void store_record(Record &record, Storer const &storer) {
    if (!record.is_dirty())
      return;

    storer(record.key(), record.data());

    record.set_dirty(false);
  }

  void store_record(Record &record) {
    if (!record.is_dirty())
      return;

    enqueue_dirty_record_(record);
  }

  std::forward_list<Record *> shift_dirty_records(std::size_t const n) {
    std::forward_list<Record *> dirties;
    for (std::size_t i = 0; i < n; ++i) {
      std::unique_lock<tbb::spin_mutex> lck(dirty_records_mutex_);
      // AutoLock lck(dirty_records_mutex_);
      if (dirty_records_.empty())
        break;
      dirties.push_front(dirty_records_.front());
      dirty_records_.pop_front();
    }
    return dirties;
  }

  std::forward_list<Record *> shift_all_dirty_records() {
    std::forward_list<Record *> dirties;
    {
      std::unique_lock<tbb::spin_mutex> lck(dirty_records_mutex_);
      // AutoLock lck(dirty_records_mutex_);
      std::swap(dirties, dirty_records_);
    }
    return dirties;
  }

protected:
  void enqueue_dirty_record_(Record &record) {
    record.set_dirty(false);
    std::unique_lock<tbb::spin_mutex> lck(dirty_records_mutex_);
    // AutoLock lck(dirty_records_mutex_);
    dirty_records_.push_front(&record);
  }

private:
  soci::session *sql_;

  Records records_;

  tbb::spin_mutex             dirty_records_mutex_;
  // LockSpin                    dirty_records_mutex_;
  std::forward_list<Record *> dirty_records_;

  std::unique_ptr<std::thread> records_writer_;
};

#endif
