#ifndef PROBLEM_SRC_BG_WRITER_H_
#define PROBLEM_SRC_BG_WRITER_H_

#include <atomic>
#include <memory>

#include <boost/tuple/tuple.hpp>

#include "LockedRecordsManager.hxx"

class BGWriter {
public:
  typedef std::string        Key;

private:
  typedef LockedRecordsManager<Key> LockedRecordsManager_;

public:
  BGWriter(soci::session         *sql,
           LockedRecordsManager_ &record_manager)
  : sql_(sql)
  , record_manager_(record_manager)
  , is_finishing_(false)
  , is_forcing_(false)
  {
    // sql_->set_log_stream(&std::cerr);
  }

  void operator () () {
    try {
      while (true) {
        if (is_finishing_ && is_forcing_)
          break;

        auto dirties(record_manager_.shift_all_dirty_records());

        if (is_finishing_ && dirties.empty())
          break;

        dirties.unique();

        std::size_t was_written = 0;
        auto start = std::chrono::high_resolution_clock::now();
        (*sql_) << "DROP TABLE IF EXISTS records_tmp";
        (*sql_) << "CREATE TEMP TABLE records_tmp (key varchar(255), data text, updated boolean default 'f')";
        while (!dirties.empty()) {
          std::ostringstream oss;
          oss << "INSERT INTO records_tmp (key, data) VALUES ";
          for (std::size_t i = 0; i < 1000 && !dirties.empty(); ++i) {
            auto const &r = dirties.front();

            {
              LockedRecordsManager_::RecordConstAccessor accessor;
              record_manager_.load_record(r->key(), accessor);
              Record &record = *accessor->second;

              record.set_dirty(false);

              oss << "('";
              std::for_each(record.key().cbegin(), record.key().cend(), [&oss](char const c) {
                if (c == '\'')
                  oss << "''";
                else
                  oss << c;
              });
              oss << "', '";
              std::for_each(record.data().cbegin(), record.data().cend(), [&oss](char const c) {
                if (c == '\'')
                  oss << "''";
                else
                  oss << c;
              });
            }
            oss << "'),";
            dirties.pop_front();
            ++was_written;
          }
          std::string q = oss.str();
          q.erase(q.end() - 1);
          (*sql_) << q;
        }
        (*sql_) << "CREATE INDEX records_key_tmp_idx ON records_tmp USING hash (key)";
        (*sql_) << "UPDATE records_tmp SET updated = 't' FROM records WHERE records.key = records_tmp.key";
        (*sql_) << "UPDATE records SET data = records_tmp.data FROM records_tmp WHERE records.key = records_tmp.key";
        (*sql_) << "INSERT INTO records SELECT key, data FROM records_tmp WHERE records_tmp.updated = 'f'";
        auto end = std::chrono::high_resolution_clock::now();
        if (was_written > 0)
          std::cout << "BGWriter: " << was_written << " record have been written for the " << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " microseconds." << std::endl;

        std::this_thread::sleep_for(std::chrono::milliseconds(100));
      }
    } catch (std::exception const &ex) {
      std::cerr << "BGWriter exception: " << ex.what() << std::endl;
    }
  }

  void finish(bool const wait = true) {
    is_finishing_ = true;
    is_forcing_   = !wait;
  }

private:
  std::unique_ptr<soci::session> sql_;

  LockedRecordsManager_ &record_manager_;

  std::atomic<bool> is_finishing_;
  std::atomic<bool> is_forcing_;
};

#endif
