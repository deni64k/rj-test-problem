#include <exception>
#include <iomanip>
#include <iostream>
#include <ostream>
#include <string>
#include <unordered_map>

#include <soci.h>
#include <soci-postgresql.h>

#include "Database.hxx"
#include "LockStats.hxx"

#define TRACE //{std::cerr << __FUNCTION__ << ":" << __LINE__ << std::endl;}

struct UserBattle {
  UserBattle(DatabaseClient &dbc, std::size_t const player_one, std::size_t const player_two)
  : dbc_(dbc)
  , player_one_(player_one)
  , player_two_(player_two)
  {}

  void operator () () {
    try {
      {
        typedef std::string Key;
        typedef LockedRecordsManager<Key> LockedRecordsManager_;

        LockedRecordsManager_::RecordAccessor accessor_one_hp;
        LockedRecordsManager_::RecordAccessor accessor_two_hp;
        dbc_.record_by_key(std::string("users:") + std::to_string(player_one_) + ":hp", accessor_one_hp);
        dbc_.record_by_key(std::string("users:") + std::to_string(player_two_) + ":hp", accessor_two_hp);

        Record &rec_one_hp = *accessor_one_hp->second;
        Record &rec_two_hp = *accessor_two_hp->second;
        int one_hp = std::stoi(rec_one_hp.data());
        int two_hp = std::stoi(rec_two_hp.data());

        LockedRecordsManager_::RecordAccessor accessor_one_atk;
        LockedRecordsManager_::RecordAccessor accessor_two_atk;
        dbc_.record_by_key(std::string("users:") + std::to_string(player_one_) + ":atk", accessor_one_atk);
        dbc_.record_by_key(std::string("users:") + std::to_string(player_two_) + ":atk", accessor_two_atk);

        Record &rec_one_atk = *accessor_one_atk->second;
        Record &rec_two_atk = *accessor_two_atk->second;
        int one_atk = std::stoi(rec_one_atk.data());
        int two_atk = std::stoi(rec_two_atk.data());

        one_hp -= two_atk;
        two_hp -= one_atk;

        rec_one_hp.set_data(std::to_string(one_hp));
        rec_two_hp.set_data(std::to_string(two_hp));

        dbc_.store_record(rec_one_hp);
        dbc_.store_record(rec_two_hp);
      }
    } catch (std::exception const &ex) {
      std::cerr << "UserBattle exception: " << ex.what() << std::endl;
    }
  }

private:
  DatabaseClient &dbc_;
  std::size_t player_one_;
  std::size_t player_two_;
};

struct BattlesThread {
  BattlesThread(DatabaseClient *dbc, std::size_t battles_total, std::size_t s, std::size_t step)
  : dbc_(dbc)
  , battles_total_(battles_total)
  , s_(s)
  , step_(step)
  {}

  void operator () () {
    try {
      for (std::size_t i = s_; i < std::min(s_ + step_, battles_total_); ++i) {
        UserBattle(*dbc_, i, (i + 1) % battles_total_)();
      }
    } catch (std::exception const &ex) {
      std::cerr << "BattlesThread exception: " << ex.what() << std::endl;
    }
  }

private:
  DatabaseClient *dbc_;
  std::size_t battles_total_;
  std::size_t s_;
  std::size_t step_;
};

int main(int argc, char const *argv[])
{
  #define DUMPVAR(x) { std::cout << std::setw(80) << #x " //=> " << (x) << std::endl; }
  std::cout << "A bunch of interesting sizeof-s:" << std::endl;
  DUMPVAR(sizeof(Lock));
  DUMPVAR(sizeof(LockSpin));
  DUMPVAR(sizeof(LockMutex));
  DUMPVAR(sizeof(std::atomic_flag));
  DUMPVAR(sizeof(std::mutex));
  DUMPVAR(sizeof(std::unordered_map<Database::Key, Lock *>));
  DUMPVAR(sizeof(std::unordered_map<Database::Key, std::unique_ptr<Lock>>));
  DUMPVAR(sizeof(tbb::concurrent_hash_map<Database::Key, Lock *>))
  DUMPVAR(sizeof(tbb::concurrent_hash_map<Database::Key, std::unique_ptr<Lock>>))
  std::cout << std::endl;
  #undef DUMPVAR

  try {
    std::string const host = argc > 1 ? argv[1] : "localhost";
    std::string connstr;
    connstr += "host=";
    connstr += host;
    connstr += " user=rocketjump password=rocketjump dbname=rocketjump";

    Database db(soci::postgresql, connstr);

    std::size_t users_total = 1000000;
    db.run_bgwriter();
    db.populate(users_total);
    db.preload(users_total);

    std::vector<std::unique_ptr<std::thread>>    threads;
    std::vector<std::unique_ptr<DatabaseClient>> dbcs;
    auto const threads_count = std::thread::hardware_concurrency() * 2;
    std::cout << "We will use " << threads_count << " threads in this test." << std::endl;
    std::size_t const battles_total = users_total;
    std::size_t const step          = battles_total / threads_count;

    auto start = std::chrono::high_resolution_clock::now();
    for (std::size_t s = 0; s < battles_total; s += step) {
      DatabaseClient *dbc = db.make_client(soci::postgresql, connstr);
      dbcs.emplace_back(dbc);

      threads.emplace_back(new std::thread(BattlesThread(dbc, battles_total, s, step)));
    }

    std::for_each(threads.begin(), threads.end(), std::mem_fn(&std::thread::join));
    auto end = std::chrono::high_resolution_clock::now();
    std::cout << "All battles was performed in " << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " microseconds." << std::endl;

    db.finish_bgwriter(false);

    LockStats::print(std::cout);

  } catch (std::exception const &e) {
    std::cerr << "Error: " << e.what() << '\n';
  }

  return 0;
}
