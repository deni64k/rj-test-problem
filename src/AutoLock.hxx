#ifndef PROBLEM_SRC_AUTO_LOCK_H_
#define PROBLEM_SRC_AUTO_LOCK_H_

#include "Lock.hxx"

class AutoLock {
public:
  AutoLock(Lock &lock, bool is_locked_already = false)
  : lock_(lock)
  , locked_(true)
  {
    if (!is_locked_already)
      lock_.lock();
  }
  AutoLock(AutoLock &&other)
  : lock_(other.lock_)
  {}
  ~AutoLock() {
    unlock();
  }
  AutoLock(AutoLock const &) = delete;

  void unlock() {
    if (locked_)
      lock_.unlock();
    locked_ = false;
  }

private:
  Lock &lock_;
  bool locked_;
};

#endif
