#ifndef PROBLEM_SRC_LOCK_STATS_H_
#define PROBLEM_SRC_LOCK_STATS_H_

#include <atomic>
#include <ostream>

struct LockStats {
  static std::atomic<std::size_t> spinning_count;
  static std::atomic<std::size_t> spinlocks_aquired;

  static void print(std::ostream &os) {
    os << "Statistic about LockSpin:" << std::endl;
    os << "LockStats::spinning_count is " << spinning_count << std::endl;
    os << "LockStats::spinlocks_aquired is " << spinlocks_aquired << std::endl;
    os << "LockStats::spinning_count / LockStats::spinlocks_aquired is " << static_cast<float>(LockStats::spinning_count) / spinlocks_aquired << std::endl;
  }
};

#endif
