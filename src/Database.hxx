#ifndef PROBLEM_SRC_DATABASE_H_
#define PROBLEM_SRC_DATABASE_H_

#include <string>
#include <memory>

#include "BGWriter.hxx"
#include "Record.hxx"
#include "LockedRecordsManager.hxx"

class DatabaseClient {
public:
  typedef std::string Key;
  typedef std::string Data;

private:
  typedef LockedRecordsManager<Key> LockedRecordsManager_;

public:
  DatabaseClient(soci::backend_factory const &backend, std::string const &connect_string,
                 LockedRecordsManager_       &record_manager)
  : sql_(new soci::session(backend, connect_string))
  , record_manager_(record_manager)
  {}

  bool record_by_key(Key const &key, LockedRecordsManager_::RecordAccessor &accessor) {
    return record_manager_.load_record(key, accessor, [&] (Key const &key) -> LockedRecordsManager_::RecordLoaderResult {
      Data data;
      soci::indicator ind;
      (*sql_) << "SELECT data FROM records WHERE key = :key",
        soci::into(data, ind), soci::use(key);

      return LockedRecordsManager_::RecordLoaderResult(data, ind != soci::i_ok);
    });
  }

  void store_record(Record &record) {
    record_manager_.store_record(record);
  }

private:
  std::unique_ptr<soci::session> sql_;

  LockedRecordsManager_ &record_manager_;
};

class Database {
public:
  typedef std::string Key;
  typedef std::string Data;

private:
  typedef LockedRecordsManager<Key> LockedRecordsManager_;

public:
  Database()
  : sql_(nullptr)
  , record_manager_(nullptr)
  {
  }

  Database(soci::backend_factory const &backend, std::string const &connect_string)
  : sql_(new soci::session(soci::postgresql, connect_string))
  , record_manager_(sql_.get())
  , bgwriter_(new BGWriter(new soci::session(backend, connect_string), record_manager_))
  {
    // sql_->set_log_stream(&std::cerr);
  }

  DatabaseClient * make_client(soci::backend_factory const &backend, std::string const &connect_string) {
    DatabaseClient *client = new DatabaseClient(backend, connect_string, record_manager_);
    return client;
  }

  ~Database()
  {
    finish_bgwriter();
  }

  void run_bgwriter() {
    bgwriter_thread_.reset(new std::thread(std::bind(&BGWriter::operator (), std::ref(*bgwriter_))));
  }

  void finish_bgwriter(bool const wait = true) {
    if (!(bgwriter_thread_ && bgwriter_thread_->joinable()))
      return;

    bgwriter_->finish(wait);

    if (wait)
      bgwriter_thread_->join();
    else
      bgwriter_thread_->detach();
  }

  void connect(soci::backend_factory const &backend, std::string const &connect_string)
  {
    sql_.reset(new soci::session(backend, connect_string));
  }

  std::size_t count() {
    std::size_t count;
    (*sql_) << "SELECT count(*) FROM records", soci::into(count);
    return count;
  }

  void populate(std::size_t size = 100) {
    std::string const data("Some private data for key ");

    bool to_create_index = false;
    try {
      if (count() >= size * 3)
        return;
    } catch (soci::soci_error const &) {
      // Ignore when table does not exists.
      (*sql_) << "CREATE TABLE records (key varchar(255), data text)";
      to_create_index = true;
    }

    std::size_t const step = 1000;
    std::size_t i = count(), j;
    while (i < size) {
      j = std::min(i + step, size);
      (*sql_) << "INSERT INTO records SELECT ('users:' || i) AS key, ('user' || i || '@ya.ru') AS data FROM generate_series(" << i << ", " << (j - 1) << ") i";
      (*sql_) << "INSERT INTO records SELECT ('users:' || i || ':hp') AS key, '100' AS data FROM generate_series(" << i << ", " << (j - 1) << ") i";
      (*sql_) << "INSERT INTO records SELECT ('users:' || i || ':atk') AS key, '10' AS data FROM generate_series(" << i << ", " << (j - 1) << ") i";
      i = j;
    }

    if (to_create_index)
      (*sql_) << "CREATE INDEX records_key_idx ON records USING hash (key)";

    (*sql_) << "VACUUM ANALYZE records";
  }

  void drop() {
    (*sql_) << "DROP TABLE IF EXISTS records";
  }

  void preload(std::size_t size = 1000) {
    record_manager_.preload(size * 3);
  }

  soci::session & sql() { return *sql_; }
  soci::session const & sql() const { return *sql_; }

private:
  std::unique_ptr<soci::session> sql_;

  LockedRecordsManager_ record_manager_;

  std::unique_ptr<BGWriter>    bgwriter_;
  std::unique_ptr<std::thread> bgwriter_thread_;
};

#endif
