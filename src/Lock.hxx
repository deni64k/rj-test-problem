#ifndef PROBLEM_SRC_LOCK_H_
#define PROBLEM_SRC_LOCK_H_

#include <atomic>

#include "LockMutex.hxx"
#include "LockSpin.hxx"

typedef LockSpin Lock;

#endif
